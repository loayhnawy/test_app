import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/ui/home.dart';
import 'package:test_app/services/Auth.dart';

class Index extends StatefulWidget {
  @override
  _IndexState createState() => _IndexState();
}

class _IndexState extends State<Index> {
  @override
  void initState() {
    Future.delayed(
      Duration(milliseconds: 1500),
          () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Home(),
        ),
      ),
    );
    show();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // ignore: missing_return
      body: TextButton(
        onPressed: () {
          return Future.delayed(
            Duration(milliseconds: 30),
                () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => Home(),
              ),
            ),
          );
        },
        child: Center(
          child: Hero(
            tag: 'logo',
            child: Container(
              width: 200,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(400),
                child: Image.asset('images/ic_launcher.png',),

              ),
            ),
          ),
        ),
      ),
    );
  }

  Future show() async {
    await Provider.of<Auth>(context, listen: false).display_post();
  }
}
