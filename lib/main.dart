import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/services/Auth.dart';
import 'index.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Auth()),
      ],
      child: MyApp(),
    ),
  );

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // primarySwatch: Colors.teal,
      ),
      home: Index(),
    );
  }
}

