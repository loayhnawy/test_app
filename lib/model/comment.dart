// To parse this JSON data, do
//
//     final memeRepo = memeRepoFromJson(jsonString);

import 'dart:convert';

ListComment memeRepoFromJson(String str) => ListComment.fromJson(json.decode(str));

String memeRepoToJson(ListComment data) => json.encode(data.toJson());

class ListComment {
  ListComment({
     this.success,
     this.data,
  });

  bool success;
  Data data;

  factory ListComment.fromJson(Map<String, dynamic> json) => ListComment(
        success: json["success"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
      };
}

class Data {
  Data({
     this.memes,
  });

  List<Comment> memes;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        memes: List<Comment>.from(json["memes"].map((x) => Comment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "memes": List<dynamic>.from(memes.map((x) => x.toJson())),
      };
}

class Comment {
  Comment({
    this.id,
    this.name,
    this.url,
    this.width,
    this.height,
    this.boxCount,
  });

  String id;
  String name;
  String url;
  int width;
  int height;
  int boxCount;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["id"],
        name: json["name"],
        url: json["url"],
        width: json["width"],
        height: json["height"],
        boxCount: json["box_count"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "url": url,
        "width": width,
        "height": height,
        "box_count": boxCount,
      };
}
