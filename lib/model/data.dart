import 'package:test_app/model/post.dart';

class Data{

  List<Post> posts ;

  Data({this.posts});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    posts : List<Post>.from(json["children"].map((x) => Post.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "children": List<dynamic>.from(posts.map((x) => x.toJson())),
  };

}