class DataPost
{
  String author;
  int  ups;
  String selftext;
  String title;
  int num_comments;
  String url;

  DataPost(this.author,this.ups,this.num_comments,this.selftext,this.title,this.url);

  DataPost.fromJson(Map<String, dynamic> json)
      : author = json['author'],
         ups = json['ups'],
         selftext = json['selftext'],
        title = json['title'],
        url = json['url'],
        num_comments = json['num_comments'];

}