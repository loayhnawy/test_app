import 'package:test_app/model/listPost.dart';

class ListComment
{
  List<ListPost> comment ;

  ListComment({this.comment});

  factory ListComment.fromJson(Map<String, dynamic> json) => ListComment(
    comment : List<ListPost>.from(json[''].map((x) => ListPost.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    '': List<dynamic>.from(comment.map((x) => x.toJson())),
  };

}