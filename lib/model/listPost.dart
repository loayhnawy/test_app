import 'package:test_app/model/data.dart';

class ListPost{

  String kind ;
  Data data ;

  ListPost({this.kind, this.data});

  ListPost.fromJson(Map<String, dynamic> json)
      :   kind  = json['kind'],
          data  = Data.fromJson(json['data']);

  Map<String, dynamic> toJson() => {
    "kind": kind,
    "date": data,

  };

}