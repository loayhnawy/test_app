import 'package:test_app/model/dataPost.dart';

class Post
{
  String kind;
  DataPost date;

  Post(this.kind,this.date);

  Post.fromJson(Map<String, dynamic> json)
      : kind = json['kind'],
        date =  DataPost.fromJson(json['data']);

  Map<String, dynamic> toJson() => {
    "kind": kind,
    "date": date,

  };

}