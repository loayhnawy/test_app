import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';
import 'package:test_app/model/listPost.dart';
import 'package:test_app/model/comment.dart';
import 'package:test_app/model/post.dart';
import 'dio_helper.dart';

class Auth extends ChangeNotifier {

  bool night = true;
  List<Post> posts = [];
  List<Comment> comments = [];

  Future<ListPost> display_post() async {
    try {
      final Dio.Response response = await dio().get(
        'https://www.reddit.com/r/rust/.json',);
      ListPost listPost = ListPost.fromJson(response.data);
      posts = listPost.data.posts;

      notifyListeners();
      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return listPost;
    } catch (e) {
      print(e);
    }
  }

  Future<ListComment> display_comment() async {
    try {
      final Dio.Response response = await dio().get('https://api.imgflip.com/get_memes');
      ListComment listComment = ListComment.fromJson(response.data);
      comments = listComment.data.memes;

      notifyListeners();
      print(
          "===================================================================================================");
      print(response.data.toString());
      print(
          "===================================================================================================");
      return listComment;
    } catch (e) {
      print(e);
    }
  }

}
