import 'package:dio/dio.dart' ;

Dio dio() {
  Dio dio = new Dio();

  dio.options.baseUrl = "http://192.168.1.105:8079/api";
  dio.options.headers['Accept'] = "application/json";

  return dio;
}