import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:test_app/services/Auth.dart';
import 'package:test_app/ui/shape.dart';

import 'home.dart';

class Details extends StatefulWidget {
  String auther;
  int date;
  String postContent;
  int comment;

  Details(this.auther, this.date, this.postContent, this.comment);

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Consumer<Auth>(builder: (context, auth, child) {
        return Scaffold(
          // appBar: AppBar(
          //   automaticallyImplyLeading: false,
          //   backgroundColor:
          //       auth.night ? Color.fromRGBO(6, 52, 106, 30) : Colors.black54,
          //   title: Padding(
          //     padding: const EdgeInsets.only(left: 10),
          //     child: Text(
          //       'Reddit',
          //       style: TextStyle(fontSize: 17),
          //     ),
          //   ),
          //   actions: [
          //     Row(
          //       children: [
          //         TextButton(
          //           onPressed: () {
          //             Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                 builder: (context) => Home(),
          //               ),
          //             );
          //           },
          //           child: Padding(
          //             padding: const EdgeInsets.only(right: 20),
          //             child: Row(
          //               children: [
          //                 Icon(Icons.arrow_back_outlined, color: Colors.white),
          //                 SizedBox(
          //                   width: 10,
          //                 ),
          //                 Text(
          //                   'Back',
          //                   style: TextStyle(fontSize: 15, color: Colors.white),
          //                 ),
          //               ],
          //             ),
          //           ),
          //         ),
          //       ],
          //     )
          //   ],
          // ),
          body:
          Container(
            height: double.infinity,
            width: double.infinity,
            color: auth.night ? Colors.white : Colors.black,
            child: Column(children: [
              Container(
              height: 130,
               child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color:auth.night ? Color.fromRGBO(6, 52, 106, 30) : Colors.white12,
                    ),

                  ),
                Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    child: Container(
                      child: CustomPaint(
                        painter: CustomContainerShapeBorder(
                          height: 45,
                          width: 420,
                          radius: 50.0,
                          fillColor: auth.night ? Colors.white : Colors.black,
                        ),
                      ),
                    ),
                  ),

                  Row(
                     children: [
                       Padding(
                         padding: const EdgeInsets.all(30),
                         child: Text(
                          'Reddit',
                          style: TextStyle(fontSize: 18,color: Colors.white),
                          ),
                       ),
                       Padding(
                         padding: const EdgeInsets.only(left:150),
                         child: Row(
                           children: [
                             TextButton(
                               onPressed: () {
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(
                                     builder: (context) => Home(),
                                   ),
                                 );
                               },
                               child: Padding(
                                 padding: const EdgeInsets.only(right: 10),
                                 child: Row(
                                   children: [
                                     Icon(Icons.arrow_back_outlined, color: Colors.white),
                                     SizedBox(
                                       width: 10,
                                     ),
                                     Text(
                                       'Back',
                                       style: TextStyle(fontSize: 11, color: Colors.white),
                                     ),
                                   ],
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ),
                     ],
                   ),
                ],
              ),
            ),
              Row(
                children: [
                  SizedBox(
                    width: 230,
                  ),
                  Text(
                    'Hot Posts',
                    style: TextStyle(
                        color: Color.fromRGBO(140, 0, 0, 1), fontSize: 17),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(
                    Icons.local_fire_department_sharp,
                    color: Color.fromRGBO(140, 0, 0, 1),
                    size: 35,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                child: Container(
                    decoration: BoxDecoration(
                      color: auth.night ?Colors.white:Colors.white12,
                      boxShadow: [
                        BoxShadow(
                            blurRadius:4, spreadRadius: 3, color: Colors.black12)
                      ],
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 20),
                        child: Row(
                          children: [
                            Text(
                                widget.auther,
                                style: TextStyle(
                                  color: auth.night
                                      ? Colors.black38
                                      : Colors.white30,
                                  fontSize: 14,
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 2),
                        child: Row(
                          children: [
                            Text(widget.date.toString()+ ' days ago',
                                style: TextStyle(
                                  color: auth.night
                                      ? Colors.black38
                                      : Colors.white30,
                                  fontSize: 14,
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),

                           child: Text(
                            widget.postContent,
                                style: TextStyle(
                                  color:
                                      auth.night ? Colors.black : Colors.white,
                                  fontSize: 14,
                                )),

                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 280, top: 20, bottom: 10),
                        child: Row(
                          children: [
                            Padding(
                                padding: const EdgeInsets.all(5),
                                child: Icon(
                                  Icons.mode_comment_outlined,
                                  color: auth.night
                                      ? Colors.black38
                                      : Colors.white30,
                                  size: 25,
                                )),
                            Text(
                              widget.comment.toString(),
                              style: TextStyle(
                                color: auth.night
                                    ? Colors.black38
                                    : Colors.white30,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ])),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0,20,220,20),
                child: Text('Comment',style: TextStyle(
                    color: auth.night? Colors.black:Colors.white ,
                    fontSize: 15),),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: auth.comments == null?  0:widget.comment,
                    itemBuilder: (ctx, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 20,top:5),
                            child: Row(
                              children: [
                                ClipRRect(
                                      borderRadius: BorderRadius.circular(30),
                                      child:
                                      Image.network(
                                        index<90 ?auth.comments[index+1].url:auth.comments[10].url,
                                        height: 60,
                                        width: 60,),
                                      ),
                                     SizedBox(width: 8,),
                                     Text(
                                       index<90 ? auth.comments[index+1].name: auth.comments[10].name,
                                          style: TextStyle(
                                          color: auth.night? Colors.black:Colors.white ,
                                          fontSize: 10),),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 80),
                            child: Text(
                              index < auth.posts.length ?auth.posts[index].date.selftext: auth.posts[10].date.selftext,
                                style: TextStyle(
                                color: auth.night? Colors.black38 :Colors.white54 ,
                                fontSize: 10),),
                          ),
                          Text('_____________________________________________',style: TextStyle(
                              color: auth.night? Colors.black38:Colors.white ,
                              fontSize: 10),),
                          SizedBox(height: 10,),
                        ],
                      );
                    }),
              ),
            ]),
        ));
      }),
    );
  }
}

