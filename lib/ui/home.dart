import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/model/listPost.dart';
import 'package:test_app/services/Auth.dart';
import 'package:test_app/ui/shape.dart';
import 'package:test_app/ui/details.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(child: SafeArea(
        child:Consumer<Auth>(builder: (context, auth, child) {
      return Scaffold(
        // appBar: AppBar(
        //   automaticallyImplyLeading: false,
        //   backgroundColor: auth.night ?Color.fromRGBO(6, 52, 106, 30):Colors.black54,
        //   title: Padding(
        //     padding: const EdgeInsets.only(left: 10),
        //     child: Text(
        //       'Reddit',
        //       style: TextStyle(fontSize: 17),
        //     ),
        //   ),
        //   actions: [
        //     TextButton(
        //       onPressed: () {
        //         setState((){
        //             auth.night= !auth.night;
        //             print(auth.night);
        //           },
        //         );
        //       },
        //       child: Padding(
        //         padding: const EdgeInsets.only(right: 25),
        //         child: Icon(
        //               auth.night
        //                   ? Icons.nightlight_round
        //                   : Icons.wb_sunny_outlined,
        //                    color: Colors.white)
        //       ),
        //     )
        //   ],
        // ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:auth.night? Colors.white:Colors.black,
          child: Column(
            children: [
              Container(
            height: 130,
              child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color:auth.night ? Color.fromRGBO(6, 52, 106, 30) : Colors.white12,
                  ),

                ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  child: Container(
                    child: CustomPaint(
                      painter: CustomContainerShapeBorder(
                        height: 45,
                        width: 420,
                        radius: 50.0,
                        fillColor: auth.night ? Colors.white : Colors.black,
                      ),
                    ),
                  ),
                ),

                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(25),
                      child: Text(
                        'Reddit',
                        style: TextStyle(fontSize: 18,color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:190),
                      child:  TextButton(
                          onPressed: () {
                          setState((){
                          auth.night= !auth.night;
                          },
                          );
                          },
                          child: Padding(
                          padding: const EdgeInsets.only(right: 25),
                          child: Icon(
                          auth.night
                          ? Icons.nightlight_round
                              : Icons.wb_sunny_outlined,
                          color: Colors.white,size: 35,)
                          ),
                          ),
                            ),
                  ],
                ),
              ],
            ),
          ),
              Row(
                children: [
                  SizedBox(width: 230,),
                  Text('Hot Posts',style: TextStyle(color:Color.fromRGBO(140, 0, 0, 1) ,fontSize: 17),),
                  SizedBox(width: 5,),
                  Icon(
                    Icons.local_fire_department_sharp,
                    color: Color.fromRGBO(140, 0, 0, 1),
                    size: 37,
                  ),
                ],
              ),
              SizedBox(height: 7,),
              Expanded(
                child: ListView.builder(
                  itemCount: auth.posts == null ?  0 : auth.posts.length,
                    itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: TextButton(
                        onPressed: () async{
                          await Provider.of<Auth>(context, listen: false).display_comment();
                          if(auth.posts[index].date.num_comments != 0) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Details(
                                        auth.posts[index].date.author,
                                        auth.posts[index].date.ups,
                                        auth.posts[index].date.title,
                                        auth.posts[index].date.num_comments
                                       ),
                                  ),
                                );
                          }
                        },
                        child: auth.posts == null?  CircularProgressIndicator(backgroundColor:Colors.blue,): Container(
                         decoration: BoxDecoration(
                           color: auth.night ?Colors.white:Colors.white12,
                            boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                spreadRadius: 3,
                                color: Colors.black12)
                          ],
                             borderRadius: BorderRadius.circular(30),
                           ),
                            child: Column(mainAxisSize: MainAxisSize.max, children: [
                                   Padding(
                                padding: const EdgeInsets.only(left: 20, top: 20),
                                child: Row(
                                  children: [
                                    Text(
                                        auth.posts[index].date.author,
                                        style: TextStyle(
                                          color: auth.night ? Colors.black38 : Colors.white30,
                                          fontSize: 14,
                                        )),
                                  ],
                                ),
                              ),
                                   Padding(
                                padding: const EdgeInsets.only(left: 20, top: 2),
                                child:
                                Row(
                                  children: [
                                    Text(
                                         auth.posts[index].date.ups.toString()+ ' days ago',
                                        style: TextStyle(
                                          color: auth.night ? Colors.black38 : Colors.white30,
                                          fontSize: 14,
                                        )),
                                  ],
                                ),
                              ),
                                   Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                     child: Text(
                                     auth.posts[index].date.title,
                                        style: TextStyle(
                                          color: auth.night ? Colors.black : Colors.white,
                                          fontSize: 14,
                                        )),

                              ),
                                   Padding(
                                padding: const EdgeInsets.only(
                                    left: 280, top: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: Icon(
                                          Icons.mode_comment_outlined,
                                          color:auth.night ? Colors.black38 : Colors.white30,
                                          size: 25,
                                        )),
                                    Text(
                                      auth.posts[index].date.num_comments.toString(),
                                      style: TextStyle(
                                        color: auth.night ? Colors.black38 : Colors.white30,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ])),
                      ),
                    );
                  }),
              ),
            ],
          ),
        ),
      );
    }),
    ),
        // ignore: missing_return
        onRefresh:()  {
        return  details();
         });

  }
  Future details() async
  {
      await Provider.of<Auth>(context, listen: false).display_post();
  }
}
